import requests
import json
from browserHeader import loadHeader, reloadHeader

class TwitterSearchScraper:

    def __init__(self, searchTerm, searchResultsNo = 20):
        self.link = "https://twitter.com/i/api/2/search/adaptive.json?include_profile_interstitial_type=1&include_blocking=1&include_blocked_by=1&include_followed_by=1&include_want_retweets=1&include_mute_edge=1&include_can_dm=1&include_can_media_tag=1&include_ext_has_nft_avatar=1&skip_status=1&cards_platform=Web-12&include_cards=1&include_ext_alt_text=true&include_quote_count=true&include_reply_count=1&tweet_mode=extended&include_entities=true&include_user_entities=true&include_ext_media_color=true&include_ext_media_availability=true&include_ext_sensitive_media_warning=true&send_error_codes=true&simple_quoted_tweet=true&q=" + searchTerm + "&count=" + str(searchResultsNo) + "&query_source=typed_query&pc=1&spelling_corrections=1&ext=mediaStats,highlightedLabel,hasNftAvatar,voiceInfo,superFollowMetadata"
        self.header = loadHeader()

    def getSearchResults(self):
        r = ""
        while True:
            r = requests.get(self.link, headers=self.header)
            if(r.status_code != 200):
                  self.header = reloadHeader()
            else:
                break
        return json.loads(r.text)["globalObjects"]["tweets"]


