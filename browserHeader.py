from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options
import json
import time

def getRenewableHeader():
   
   renewableHeaderNameList = ["authorization", "Referer", "sec-ch-ua", "sec-ch-ua-mobile", "sec-ch-ua-platform", "User-Agent", "x-csrf-token", "x-guest-token", "x-twitter-active-user", "x-twitter-client-language"]

   searchurl = "https://twitter.com/search?q=bank&src=typed_query"

   caps = DesiredCapabilities.CHROME
   caps['goog:loggingPrefs'] = {"performance": "ALL"}

   driver = webdriver.Chrome(executable_path="chromedriver.exe", desired_capabilities=caps)

   driver.get(searchurl)

   renewableHeader = {}
   
   while True:
      
      driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
      time.sleep(0.5)
      log = driver.get_log("performance")

      for i in range(len(log)):
         
         logDict = json.loads(log[i]['message'])
         if 'message' not in logDict.keys():
            continue
         
         if 'params' not in logDict['message'].keys():
            continue

         if 'request' not in logDict['message']['params'].keys():
            continue

         logImportant = True
         
         for headerName in renewableHeaderNameList:
            if headerName not in logDict['message']['params']['request']['headers']:
               logImportant = False
               break
            
         if logImportant:
            for headerName in renewableHeaderNameList:
               renewableHeader[headerName] = logDict['message']['params']['request']['headers'][headerName]
            break

      if len(renewableHeader.keys()) != 0:
         break

   driver.close()

   return renewableHeader

def loadHeader():
   with open('header.txt') as f:
    lines = f.readlines()
    headerStr = ""
    for line in lines:
       headerStr += str(line)
    headerStr.replace("\n","")
    headerDict = json.loads(headerStr)
    return headerDict
   
def reloadHeader():
   with open('header.txt') as f:
    lines = f.readlines()
    headerStr = ""
    for line in lines:
       headerStr += str(line)
    headerStr.replace("\n","")
    headerDict = json.loads(headerStr)
    renewableHeader = getRenewableHeader()
    for key in renewableHeader.keys():
       headerDict[key] = renewableHeader[key]
    f = open("header.txt", "w")
    f.write(json.dumps(headerDict, indent=2))
    f.close()
    return headerDict


