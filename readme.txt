Twitter search scraper capable of retrieving search results by using requests and selenium chrome driver.
A chromedriver is already attached but make sure it's the same version as your Google Chrome(the driver's version is 98).
To use, simply paste the files in your project and import the TwitterSearchScraper class to your file.

The code can be used as so:

import TwitterSearchScraper from twitterSearchScraper


print(TwitterSearchScraper(searchTerm = "banana", searchResultsNo = 10).getSearchResults())

Selenium is used to gather a token for the api. 
Selenium will run only if the result of the request with the header in header.txt is not 200.